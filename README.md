# Kendal Plugin

Kendal Plugin is a plugin for IntelliJ IDEA, it is meant to support java syntax changes done by Kendal Framework.

This project is a part of a Bachelor's diploma thesis in the field of Computer Science.

## Manual
### How to install
1. Click "File"
2. Click "Settings"
3. Select "Plugins" tab
4. Click "Browse repositories..." button
5. Search for "Kendal" 
6. Click  "Install"
7. Restart IntelliJ IDEA

## For developers
### Testing
IntelliJ IDEA plugin is a piece of software that is very difficult (or even impossible) to test automatically,
there is a collection of scenarios created for the purpose of acceptance testing. Those scenarios are designed
as manual tests, where a tester must read each scenario and perform included steps in the given order. All
the scenarios are gathered in the _acceptanceTests_ directory, separated - one file per one Kendal's feature.

## Authors

* **Konrad Gancarz** - [LinkedIn](https://www.linkedin.com/in/konrad-gancarz-238901127/)
* **Arkadiusz Ryszewski** - [LinkedIn](https://www.linkedin.com/in/arkadiusz-ryszewski-203640b9/)

## References
* Kendal Project repository: https://bitbucket.org/Diler96/kendal
* Thesis document: https://bitbucket.org/ArkadyPL/bsc-thesis

## License

This project is licensed under the MIT License - see the [MIT License](https://opensource.org/licenses/MIT) for details.
