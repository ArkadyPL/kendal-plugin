package kendal.plugin.utils

import com.intellij.psi.*

class AnnotationUtils {
    companion object {

        fun resolveStringValue(psiValue: PsiAnnotationMemberValue): String? {
            if (psiValue is PsiReferenceExpression) {
                val resolved = psiValue.resolve()
                if (resolved is PsiVariable) {
                    return resolved.computeConstantValue() as String
                }
            } else if (psiValue is PsiLiteralExpression) {
                return psiValue.value as String
            }  else if (psiValue is PsiPrefixExpression) {
                    return psiValue.getText()
            }
            return null
        }

        fun resolveTypeValue(psiValue: PsiAnnotationMemberValue) : PsiType? {
            if(psiValue is PsiClassObjectAccessExpression) {
                return psiValue.operand.type
            }
            return null
        }

        fun resolveBooleanValue(psiValue: PsiAnnotationMemberValue): Boolean? {
            if (psiValue is PsiReferenceExpression) {
                val resolved = psiValue.resolve()
                if (resolved is PsiVariable) {
                    return resolved.computeConstantValue() as Boolean
                }
            } else if (psiValue is PsiLiteralExpression) {
                return psiValue.value as Boolean
            }
            return null
        }

        fun resolveAnnotationValue(psiValue: PsiAnnotationMemberValue) : PsiAnnotation? {
            if(psiValue is PsiAnnotation) {
                return psiValue
            }
            return null
        }

        /**
         * Helper method checking if {@param annotation} inherits annotation with qualified name equal to {@param possiblyInheritedAnnotationName} by @Inherit.
         */
        fun inheritsAnnotation(annotation: PsiAnnotation?, possiblyInheritedAnnotationName : String): Boolean {
            if(annotation == null) {
                return false
            }

            val annotationClass = (annotation.nameReferenceElement?.resolve() ?: return false) as PsiClass
            if(annotationClass.qualifiedName.equals(possiblyInheritedAnnotationName)) {
                return true
            }

            val inheritAnnotation = annotationClass.getAnnotation("kendal.api.inheritance.Inherit") ?: return false
            val inheritedAnnotation = inheritAnnotation.findAttributeValue("value") as PsiAnnotation

            return inheritsAnnotation(inheritedAnnotation, possiblyInheritedAnnotationName)
        }
    }
}