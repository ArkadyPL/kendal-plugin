package kendal.plugin.extensions

import com.intellij.codeInsight.daemon.impl.HighlightInfo
import com.intellij.codeInsight.daemon.impl.HighlightInfoFilter
import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.util.text.StringUtil
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiIdentifier

class StringInterpolationHighlightInfoFilter : HighlightInfoFilter {

    companion object {
        private val ANY_NAME: String = "([^ .,!+\\-=@{}()|])+"
        private val ANY_PACKAGE: String = "([^ ,!+\\-=@{}()|])+" // here dots are allowed
        private val STRING_OR_OBJECT: String = "(java\\.lang\\.String|java\\.lang\\.Object)"
        private val PARAMETERS_WITH_STRING_OR_OBJECT: String = "\\(($ANY_NAME\\, )*$STRING_OR_OBJECT(\\, $ANY_NAME)*\\)"
        private val PARAMETERS_WITH_UNKNOWN: String = "\\(($ANY_NAME\\, )*\\?(\\, $ANY_NAME)*\\)"
        private val METHOD_CALL: String = "$ANY_NAME$PARAMETERS_WITH_STRING_OR_OBJECT"
    }

    override fun accept(highlightInfo: HighlightInfo, file: PsiFile?): Boolean {
        if (highlightInfo.severity != HighlightSeverity.ERROR) return true
        val description: String = StringUtil.notNullize(highlightInfo.description)
        if (description == "Operator '+' cannot be applied to 'java.lang.String'") {
            return false
        }
        if (description == "Incompatible types. Found: 'null', required: 'java.lang.String'") {
            return false
        }
        var pattern = Regex("^'$METHOD_CALL' in '$ANY_PACKAGE' cannot be applied to '$PARAMETERS_WITH_UNKNOWN'\$")
        if (description.matches(pattern)) {
            return false
        }
        pattern = Regex("^Cannot resolve method '$ANY_NAME\\(\\?\\)'\$")
        if (description.matches(pattern)) {
            return file?.findElementAt(highlightInfo.getStartOffset()) is PsiIdentifier
        }
        return true
    }
}