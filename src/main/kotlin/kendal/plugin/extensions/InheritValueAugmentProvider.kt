package kendal.plugin.extensions

import com.intellij.psi.JavaPsiFacade
import com.intellij.psi.PsiClass
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiMethod
import com.intellij.psi.augment.PsiAugmentProvider
import com.intellij.psi.impl.light.LightMethodBuilder
import kendal.plugin.psi.KendalLightAnnotationMethodBuilder
import java.util.*

class InheritValueAugmentProvider : PsiAugmentProvider() {

    private val inheritAnnotationName = "kendal.api.inheritance.Inherit"

    override fun <Psi : PsiElement?> getAugments(element: PsiElement, type: Class<Psi>): MutableList<Psi> {
        if (element is PsiClass && PsiMethod::class.java.isAssignableFrom(type)) {
            if (element.qualifiedName.equals(inheritAnnotationName)) {
                val method: LightMethodBuilder = KendalLightAnnotationMethodBuilder(element.manager, "value", null)
                        .setContainingClass(element)
                method.setMethodReturnType(JavaPsiFacade.getInstance(method.manager.project).elementFactory
                    .createTypeByFQClassName("java.lang.annotation.Annotation", element.resolveScope))

                method.navigationElement = element

                return Collections.singletonList(method as Psi)
            }
        }
        return Collections.emptyList()
    }
}