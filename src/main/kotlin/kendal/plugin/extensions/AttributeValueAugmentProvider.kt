package kendal.plugin.extensions

import com.intellij.psi.*
import com.intellij.psi.augment.PsiAugmentProvider
import kendal.plugin.psi.KendalLightAnnotationMethodBuilder
import java.util.*

class AttributeValueAugmentProvider : PsiAugmentProvider() {

    private val attributeAnnotationName = "kendal.api.inheritance.Attribute"

    override fun <Psi : PsiElement?> getAugments(element: PsiElement, type: Class<Psi>): MutableList<Psi> {
        if (element is PsiClass && PsiMethod::class.java.isAssignableFrom(type)) {
            if (element.qualifiedName.equals(attributeAnnotationName)) {
                val method = KendalLightAnnotationMethodBuilder(element.manager, "value", null)
                        .setContainingClass(element)
                method.setMethodReturnType(JavaPsiFacade.getInstance(method.manager.project).elementFactory.createTypeByFQClassName("java.lang.Object", element.resolveScope))

                method.navigationElement = element

                return Collections.singletonList(method as Psi)
            }
        }
        return Collections.emptyList()
    }
}