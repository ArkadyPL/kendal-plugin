package kendal.plugin.extensions

import com.intellij.psi.*
import com.intellij.psi.augment.PsiAugmentProvider
import com.intellij.psi.impl.light.LightMethodBuilder
import com.intellij.psi.impl.source.tree.java.PsiAnnotationImpl
import com.intellij.util.containers.stream
import kendal.plugin.psi.KendalLightAnnotationMethodBuilder
import kendal.plugin.utils.AnnotationUtils
import java.util.*
import java.util.stream.Collectors

class InheritAugmentProvider : PsiAugmentProvider() {

    private val inheritAnnotationName = "kendal.api.inheritance.Inherit"

    override fun <Psi : PsiElement?> getAugments(element: PsiElement, type: Class<Psi>): MutableList<Psi> {
        if (element is PsiClass && PsiMethod::class.java.isAssignableFrom(type)) {
            if (element.hasAnnotation(inheritAnnotationName)) {
                val inheritAnnotation: PsiAnnotation? = element.getAnnotation(inheritAnnotationName)
                val inheritedAnnotation: PsiAnnotation? = AnnotationUtils.resolveAnnotationValue(inheritAnnotation!!.findDeclaredAttributeValue("value")!!)
                if (inheritedAnnotation != null && inheritAnnotation is PsiAnnotationImpl) {
                    val inheritedClass: PsiElement? = inheritedAnnotation.nameReferenceElement?.resolve()
                    if (inheritedClass != null) {
                        return (inheritedClass as PsiClass).methods
                                .stream()
                                .map { method -> method as PsiAnnotationMethod }
                                .map { method ->
                                    val attrValue: PsiAnnotationMemberValue? = inheritedAnnotation.findDeclaredAttributeValue(method.name)
                                    if (attrValue != null) {
                                        val newMethod: LightMethodBuilder = KendalLightAnnotationMethodBuilder(method.manager, method.name, attrValue)
                                                .setContainingClass(element)
                                        newMethod.setMethodReturnType(method.returnType)
                                        newMethod.navigationElement = element
                                        return@map newMethod
                                    }
                                    return@map method
                                }
                                .map { method -> method as Psi }
                                .collect(Collectors.toList())
                    }
                }
            }
        }
        return Collections.emptyList()
    }
}