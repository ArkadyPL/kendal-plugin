package kendal.plugin.extensions

import com.intellij.psi.*
import com.intellij.psi.augment.PsiAugmentProvider
import com.intellij.psi.impl.light.LightMethodBuilder
import com.intellij.psi.impl.source.tree.java.PsiArrayInitializerMemberValueImpl
import kendal.plugin.psi.KendalLightAnnotationMethodBuilder
import java.util.*

/**
 * Adds node representing "onMethod" attribute to node representing @Clone class declaration in PSI tree.
 */
class CloneOnMethodAugmentProvider : PsiAugmentProvider() {


    override fun <Psi : PsiElement?> getAugments(element: PsiElement, type: Class<Psi>): MutableList<Psi> {
        if (element is PsiClass && PsiMethod::class.java.isAssignableFrom(type)) {
            if (element.qualifiedName.equals("kendal.annotations.Clone")) {
                val method: LightMethodBuilder = KendalLightAnnotationMethodBuilder(element.manager, "onMethod", PsiArrayInitializerMemberValueImpl())
                        .setContainingClass(element)
                method.setMethodReturnType(JavaPsiFacade.getInstance(method.manager.project).elementFactory.createTypeByFQClassName("java.lang.annotation.Annotation", element.resolveScope).createArrayType())

                method.navigationElement = element

                return Collections.singletonList(method as Psi)
            }
        }
        return Collections.emptyList()
    }
}