package kendal.plugin.extensions

import com.intellij.openapi.util.RecursionGuard
import com.intellij.openapi.util.RecursionManager
import com.intellij.psi.*
import com.intellij.psi.augment.PsiAugmentProvider
import com.intellij.psi.impl.light.LightMethodBuilder
import com.intellij.psi.util.PsiUtil
import com.intellij.util.containers.stream
import kendal.plugin.utils.AnnotationUtils
import java.util.*
import java.util.stream.Collectors

/**
 * Adds node representing clone method to node representing its corresponding class in PSI tree.
 */
class CloneAugmentProvider : PsiAugmentProvider() {

    private val recursionGuard : RecursionGuard = RecursionManager.createGuard("kendal.annotations.Clone")

    override fun <Psi : PsiElement?> getAugments(element: PsiElement, type: Class<Psi>): List<Psi> {
        if (element is PsiClass && PsiMethod::class.java.isAssignableFrom(type)) {
            val cloned: MutableList<Psi> = recursionGuard.doPreventingRecursion(element, true) {
                element.methods.stream()
                    .map { method -> toClonedMethodOrNull(method) }
                    .filter(Objects::nonNull)
                    .map { clonedMethod ->
                        return@map getMethod(clonedMethod!!.method, clonedMethod.psiAnnotation, element)
                    }
                    .map { e -> e as Psi }
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList())
            } ?: Collections.emptyList()

            return cloned
        }

        return Collections.emptyList()
    }

    fun getMethod(method : PsiMethod, annotation: PsiAnnotation, element: PsiClass) : LightMethodBuilder? {
        fun getName(method : PsiMethod, annotation: PsiAnnotation) : String {
            val explicitName: PsiAnnotationMemberValue? = annotation.findAttributeValue("methodName")
            if (explicitName != null && !"".equals(AnnotationUtils.resolveStringValue(explicitName))) {
                val str = AnnotationUtils.resolveStringValue(explicitName)
                if (str != null) {
                    return str
                }
            }
            return method.name + "Clone"
        }

        fun getReturnType(annotation: PsiAnnotation) : PsiType? {
            val psiType: PsiType = AnnotationUtils.resolveTypeValue(annotation.findAttributeValue("transformer") ?: return null) ?: return null
            var transformer: PsiClass? = (PsiUtil.resolveClassInType(psiType))
            while(transformer != null) {
                val returnType: PsiType? = transformer.implementsList!!.children
                        .stream()
                        .filter { impl -> impl is PsiJavaCodeReferenceElement && "kendal.annotations.Clone.Transformer".equals(impl.qualifiedName)}
                        .map { impl -> ((impl as PsiJavaCodeReferenceElement).typeParameters[1]) }
                        .findFirst()
                        .orElse(null)
                if (returnType != null) {
                    return returnType
                }
                transformer = transformer.superClass
            }
            return null
        }

        val result: LightMethodBuilder =  LightMethodBuilder(element.manager, element.language, getName(method, annotation),
                method.parameterList, method.modifierList, method.throwsList, method.typeParameterList)
                .setMethodReturnType(getReturnType(annotation) ?: return null)
                .setContainingClass(element)
        result.navigationElement = annotation
        return result
    }

    private fun toClonedMethodOrNull(method: PsiMethod): ClonedMethod? {
        val annotation = method.getAnnotation("kendal.annotations.Clone")
        if(annotation != null) {
            return ClonedMethod(method, annotation)
        }

        method.annotations.forEach { ann ->
            if(AnnotationUtils.inheritsAnnotation(ann, "kendal.annotations.Clone")) {
                return ClonedMethod(method, ann)
            }
        }

        return null
    }

    data class ClonedMethod(val method: PsiMethod, val psiAnnotation: PsiAnnotation)
}