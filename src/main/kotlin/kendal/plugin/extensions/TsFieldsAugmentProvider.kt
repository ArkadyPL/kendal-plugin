package kendal.plugin.extensions

import com.intellij.psi.*
import com.intellij.psi.augment.PsiAugmentProvider
import com.intellij.util.containers.stream
import kendal.plugin.psi.KendalLightFieldBuilder
import kendal.plugin.utils.AnnotationUtils
import java.util.*
import java.util.stream.Collectors

class TsFieldsAugmentProvider : PsiAugmentProvider() {

    override fun <Psi : PsiElement?> getAugments(element: PsiElement, type: Class<Psi>): MutableList<Psi> {
        if (element is PsiClass && PsiField::class.java.isAssignableFrom(type)) {
            return element.constructors.stream()
                .flatMap {c -> c.parameterList.parameters.stream() }
                .map { parameter ->
                    for (annotation: PsiAnnotation in parameter.annotations) {
                        if (AnnotationUtils.inheritsAnnotation(annotation,"kendal.annotations.Private")) {
                            return@map KendalLightFieldBuilder(parameter.name!!, parameter.type, annotation)
                                .setContainingClass(element)
                                .setModifiers(*getModifiers(PsiModifier.PRIVATE, annotation))
                        }
                        if (AnnotationUtils.inheritsAnnotation(annotation, "kendal.annotations.Protected")) {
                            return@map KendalLightFieldBuilder(parameter.name!!, parameter.type, annotation)
                                .setContainingClass(element)
                                .setModifiers(*getModifiers(PsiModifier.PROTECTED, annotation))
                        }
                        if (AnnotationUtils.inheritsAnnotation(annotation, "kendal.annotations.PackagePrivate")) {
                            return@map KendalLightFieldBuilder(parameter.name!!, parameter.type, annotation)
                                .setContainingClass(element)
                                .setModifiers(*getModifiers(PsiModifier.PACKAGE_LOCAL, annotation))
                        }
                        if (AnnotationUtils.inheritsAnnotation(annotation, "kendal.annotations.Public")) {
                            return@map KendalLightFieldBuilder(parameter.name!!, parameter.type, annotation)
                                .setContainingClass(element)
                                .setModifiers(*getModifiers(PsiModifier.PUBLIC, annotation))
                        }
                    }
                    return@map null
                 }
                .filter(Objects::nonNull)
                .map { e -> e as PsiField }
                .collect(Collectors.toList())
                .associateBy { psiField: PsiField? -> psiField!!.name }
                .values
                .stream()
                .map { e -> e as Psi }
                .collect(Collectors.toList())
        }

        return Collections.emptyList()
    }

    private fun getModifiers(accessModifier : String, annotation: PsiAnnotation) : Array<String>  {
        val result = mutableListOf(accessModifier)
        val makeFinal = AnnotationUtils.resolveBooleanValue(annotation.findAttributeValue("makeFinal")!!)
        if (makeFinal == true) {
            result.add(PsiModifier.FINAL)
        }
        return result.toTypedArray()
    }
}