package kendal.plugin.extensions

import com.intellij.codeInsight.daemon.impl.HighlightInfo
import com.intellij.codeInsight.daemon.impl.HighlightInfoFilter
import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.util.text.StringUtil
import com.intellij.psi.PsiAnnotation
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiIdentifier

class AnnotationHighlightInfoFilter : HighlightInfoFilter {

    override fun accept(highlightInfo: HighlightInfo, file: PsiFile?): Boolean {
        if (highlightInfo.severity != HighlightSeverity.ERROR) return true
        val description: String = StringUtil.notNullize(highlightInfo.description)
        var pattern = Regex("^Incompatible types\\. Found: '[^']*', required: 'java\\.lang\\.annotation\\.Annotation'\$")
        if (description.matches(pattern)) {
            return file?.findElementAt(highlightInfo.getStartOffset())?.parent !is PsiAnnotation
        }

        return true
    }
}