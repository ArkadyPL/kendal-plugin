package kendal.plugin.extensions

import com.intellij.codeInsight.daemon.impl.HighlightInfo
import com.intellij.codeInsight.daemon.impl.HighlightInfoFilter
import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.util.text.StringUtil
import com.intellij.psi.PsiAnnotation
import com.intellij.psi.PsiArrayInitializerMemberValue
import com.intellij.psi.PsiClass
import com.intellij.psi.PsiFile
import kendal.plugin.utils.AnnotationUtils

class AttributeRequiredArgHighlightInfoFilter : HighlightInfoFilter {

    private val attributeAnnotationName = "kendal.api.inheritance.Attribute"
    private val attributeListAnnotationName = "kendal.api.inheritance.Attribute.List"

    override fun accept(highlightInfo: HighlightInfo, file: PsiFile?): Boolean {
        if (highlightInfo.severity != HighlightSeverity.ERROR) return true
        val description: String = StringUtil.notNullize(highlightInfo.description)
        val matches = Regex("'([^']*)' missing though required").find(description) ?: return true
        if (matches.groups.size == 2) {
            val paramName = matches.groups[1]!!.value
            var psiNode = file?.findElementAt(highlightInfo.getStartOffset())
            while (psiNode != null) {
                if (psiNode is PsiAnnotation) {
                    val annotationClass = (psiNode.nameReferenceElement!!.resolve() as PsiClass)
                    return !hasAttribute(annotationClass, paramName)
                }
                psiNode = psiNode.parent
            }
        }

        return true
    }

    private fun hasAttribute(annotationClass: PsiClass, paramName : String): Boolean {
        val attributeAnnotation = annotationClass.getAnnotation(attributeAnnotationName)
        if(attributeAnnotation != null) {
            if(isAttributeForParam(attributeAnnotation, paramName)) {
                return true
            }
        }
        val attributeListAnnotation = annotationClass.getAnnotation(attributeListAnnotationName)
        if(attributeListAnnotation != null) {
            val array = attributeListAnnotation.findDeclaredAttributeValue("value") as PsiArrayInitializerMemberValue
            array.initializers.forEach { expr ->
                if(expr is PsiAnnotation) {
                    if(isAttributeForParam(expr, paramName)) {
                        return true
                    }
                }
            }
        }
        return hasAttribute(annotationClass.superClass ?: return false, paramName)
    }

    private fun isAttributeForParam(attributeAnnotation: PsiAnnotation, paramName: String): Boolean {
        return AnnotationUtils.resolveStringValue(attributeAnnotation.findDeclaredAttributeValue("name") ?: return false)
                .equals(paramName)
    }
}