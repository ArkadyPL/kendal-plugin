package kendal.plugin.extensions

import com.intellij.codeInsight.daemon.impl.HighlightInfo
import com.intellij.codeInsight.daemon.impl.HighlightInfoFilter
import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.util.text.StringUtil
import com.intellij.psi.PsiClass
import com.intellij.psi.PsiFile
import com.intellij.psi.impl.source.tree.java.PsiAnnotationImpl

class AttrReferenceHighlightInfoFilter : HighlightInfoFilter {

    override fun accept(highlightInfo: HighlightInfo, file: PsiFile?): Boolean {
        if (highlightInfo.severity != HighlightSeverity.ERROR) return true
        val description: String = StringUtil.notNullize(highlightInfo.description)
        var pattern = Regex("^Incompatible types\\. Found: 'kendal.api.inheritance.AttrReference', required: '[^']*'\$")
        if (description.matches(pattern)) {
            var psiNode = file?.findElementAt(highlightInfo.getStartOffset())
            while(psiNode != null) {
                if(psiNode is PsiAnnotationImpl) {
                    if((psiNode.nameReferenceElement!!.resolve() as PsiClass).qualifiedName.equals("kendal.api.inheritance.Attribute")) {
                        return false
                    }
                }
                psiNode = psiNode.parent
            }
        }

        return true
    }
}