package kendal.plugin.psi

import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiType
import com.intellij.psi.impl.light.LightFieldBuilder

class KendalLightFieldBuilder(name : String, type : PsiType, navigationElement: PsiElement) : LightFieldBuilder(name, type, navigationElement) {

    override fun getParent(): PsiElement? {
        return this.containingClass
    }

    override fun getContainingFile(): PsiFile? {
        return parent!!.containingFile
    }

    override fun getTextRange(): TextRange {
        return navigationElement.textRange
    }
}