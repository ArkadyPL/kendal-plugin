package kendal.plugin.psi

import com.intellij.psi.PsiAnnotationMemberValue
import com.intellij.psi.PsiAnnotationMethod
import com.intellij.psi.PsiManager
import com.intellij.psi.impl.light.LightMethodBuilder

class KendalLightAnnotationMethodBuilder(manager : PsiManager, name : String, private val defaultVal : PsiAnnotationMemberValue?) : LightMethodBuilder(manager, name), PsiAnnotationMethod {

    override fun getDefaultValue(): PsiAnnotationMemberValue? {
        return defaultVal
    }


}